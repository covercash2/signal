pub fn naive_convolute(signal_src: &[f64], impulse_response: &[f64], result: &mut [f64]) {
    signal_src.iter().enumerate().for_each(|(i, src_val)| {
        impulse_response
            .iter()
            .enumerate()
            .for_each(|(j, impulse_val)| {
                result[i + j] = result[i + j] + src_val * impulse_val;
            });
    });
}
