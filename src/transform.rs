pub fn add_signals<'a, I: Iterator<Item = &'a f64>>(signal_a: I, signal_b: I) -> Vec<f64> {
    return signal_a.zip(signal_b).map(|(a, b)| {
        a + b
    }).collect();
}
