use std::f64::consts::PI;

pub fn naive_discrete_fourier_transform(
    block: &[f64],
    real_output: &mut [f64],
    imaginary_output: &mut [f64],
    block_size: usize,
) {
    for i in 0..block_size {
        for j in 0..block_size {
            let positional_parameter = (2.0 * PI * i as f64 * j as f64) / block_size as f64;
            real_output[i] = real_output[i] + block[j] * positional_parameter.cos();
            imaginary_output[i] = imaginary_output[i] - block[j] * positional_parameter.sin();
        }
    }
}

pub fn output_magnitude(dft_real: &[f64], dft_im: &[f64], result: &mut [f64]) {
    dft_real
        .iter()
        .enumerate()
        .zip(dft_im.iter())
        .for_each(|((i, real), im)| {
            result[i] = (real.powi(2) + im.powi(2)).sqrt();
        });
}

pub fn inverse_dft(dft_real: &[f64], dft_im: &[f64], block_size: usize, result: &mut [f64]) {
    let dft_size = block_size as f64 / 2.0;
    dft_real
        .iter()
        .zip(dft_im.iter())
        .take(block_size / 2)
        .map(|(re, im)| (re / dft_size, im / dft_size))
        .enumerate()
        .for_each(|(i, (real, imag))| {
            for j in 0..block_size {
                let positional_parameter = (2.0 * PI * j as f64 * i as f64) / block_size as f64;
                result[j] = result[j] + real * positional_parameter.cos();
                result[j] = result[j] + imag * positional_parameter.sin();
            }
        });
}
