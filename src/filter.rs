use std::f64::consts::PI;

/// a simple filter that moves a window over the signal taking an average.
/// the window is center aligned
pub fn moving_average(input_src: &[f64], window_size: usize, output: &mut [f64]) {
    let input_length = input_src.len();
    // TODO confirm
    // hopefully this will floor. can't seem to find docs
    let offset: isize = window_size as isize / 2;

    for i in offset..(input_length as isize - offset - 1) {
        output[i as usize] = 0.0;

        // scan window
        for j in (-offset)..offset {
            let index = (i + j) as usize;
            output[i as usize] += input_src[index];
        }
        output[i as usize] /= window_size as f64;
    }
}

pub fn recursive_moving_average(input_src: &[f64], window_size: usize, output: &mut [f64]) {
    let mut acc = 0.0;
    for i in 0..(window_size - 1) {
        acc += input_src[i];
    }
    output[(window_size - 1) / 2] = acc / window_size as f64;

    let offset = window_size / 2;
    for i in offset..(input_src.len() - offset - 1) {
        acc += input_src[i + (window_size - 1) / 2] - input_src[i - offset];
        output[i] = acc / window_size as f64;
    }
}

pub fn windowed_sinc_low_pass(cutoff: f64, length: usize) -> Vec<f64> {
    return (0..length)
        .map(|i| match i as isize - length as isize / 2 {
            0 => 2.0 * PI * cutoff,
            x => {
                let x = x as f64;
                (2.0 * PI * cutoff * x).sin() / x * hamming_window(i, length)
            }
        })
        .collect();
}

fn hamming_window(sample_index: usize, sample_length: usize) -> f64 {
    return 0.54 - 0.46 * (2.0 * PI * sample_index as f64 / sample_length as f64).cos();
}
