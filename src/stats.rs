pub fn running_sum(signal_src: &[f64], signal_out: &mut [f64]) {
    for (i, val) in signal_src.iter().enumerate() {
        match i {
            0 => {
                signal_out[i] = *val;
            }
            _ => {
                signal_out[i] = val + signal_out[i - 1];
            }
        }
    }
}

pub fn first_difference(signal_src: &[f64], signal_out: &mut [f64]) {
    for (i, val) in signal_src.iter().enumerate() {
        match i {
            0 => {
                signal_out[i] = *val;
            }
            _ => {
                signal_out[i] = val - signal_out[i - 1];
            }
        }
    }
}

pub fn block_mean(block: &[f64], block_size: usize) -> f64 {
    return block.into_iter().sum::<f64>() / (block_size as f64);
}

pub fn block_variance(block: &[f64], block_size: usize) -> f64 {
    let (sum, sumsq) = block
        .into_iter()
        .fold((0.0, 0.0), |(mut sum, mut sumsq), val| {
            sum += val;
            sumsq += val.powi(2);
            (sum, sumsq)
        });

    return (sumsq - (sum.powi(2) / block_size as f64)) / ((block_size - 1) as f64);
}

pub fn block_standard_deviation(block: &[f64], block_size: usize) -> f64 {
    return block_variance(block, block_size).sqrt();
}

/// root mean squared.
/// the root of the mean of the squares of the values.
pub fn block_rms(block: &[f64], block_size: usize) -> f64 {
    return (block.into_iter().fold(0.0, |mut sum, val| {
        sum += val * val;
        sum
    }) / block_size as f64)
        .sqrt();
}
